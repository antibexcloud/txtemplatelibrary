﻿if not exists (select * from sysobjects where name='Label' and xtype='U')
CREATE TABLE dbo.Label ( 
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(32) not null,
    [Modified] datetime not null DEFAULT(getdate()),

	CONSTRAINT [PK_Label] PRIMARY KEY ([Id]) ON [PRIMARY]
)
GO

if not exists (select * from sysobjects where name='TextTemplate' and xtype='U')
CREATE TABLE dbo.TextTemplate ( 
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(150) not null,
    [Description] nvarchar(max) not null,
	[TemplateArchive] varbinary(max) null,
    [Modified] datetime not null DEFAULT(getdate()),

	CONSTRAINT [PK_TextTemplate] PRIMARY KEY ([Id]) ON [PRIMARY]
)
GO


if not exists (select * from sysobjects where name='TextTemplateLabel' and xtype='U')
CREATE TABLE dbo.TextTemplateLabel ( 
    [Id] int IDENTITY(1,1) NOT NULL,

    [IdTemplate] int not null,
    [IdLabel] int not null,

    [Modified] datetime not null DEFAULT(getdate()),

	CONSTRAINT [FK_TextTemplateLabel_Template] FOREIGN KEY ([IdTemplate]) REFERENCES  dbo.TextTemplate ([Id]),
	CONSTRAINT [FK_TextTemplateLabel_Label] FOREIGN KEY ([IdLabel]) REFERENCES  dbo.Label ([Id]),

	CONSTRAINT [PK_TextTemplateLabel] PRIMARY KEY ([Id]) ON [PRIMARY]
)
GO

if not exists (select * from sysobjects where name='AuditLog' and xtype='U')
CREATE TABLE dbo.AuditLog ( 
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime not null DEFAULT(getdate()),
    [Host] nvarchar(100) not null,
    [User] nvarchar(100) not null,

    [Table] nvarchar(32) not null,
    [Operation] nvarchar(32) not null,
    [Description] nvarchar(max) not null,

	CONSTRAINT [PK_AuditLog] PRIMARY KEY ([Id]) ON [PRIMARY]
)
GO

if (select count(*) from dbo.[Label]) = 0
begin
  insert [Label] ([Name]) values('All')
  insert [Label] ([Name]) values('EHC')
  insert [Label] ([Name]) values('MVA')
  insert [Label] ([Name]) values('Soap notes')
  insert [Label] ([Name]) values('Other')
end;
GO
