﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Uo.Client.TxTemplateLib
{
    public static class TemplateLibClient
    {
        public static TemplateLibInternalClient GetClient()
        {
            var address = ConfigurationManager.AppSettings["TxTemplateLibUrl"];
            if (string.IsNullOrEmpty(address)) throw new Exception("TxTemplateLibUrl key is empty!!!");

            var isHttp = address.ToLower().StartsWith("http:");
            var isTcp = address.ToLower().StartsWith("net.tcp:");

            var sLog = ConfigurationManager.AppSettings["TxTemplateLibLogin"] ?? string.Empty;
            var sPsw = ConfigurationManager.AppSettings["TxTemplateLibPassword"] ?? string.Empty;
            if (string.IsNullOrEmpty(sLog) || string.IsNullOrEmpty(sPsw))
                throw new Exception("TxTemplateLibLogin or TxTemplateLibPassword key is empty!!!");

            var openTimeout = 1;
            var closeTimeout = 1;
            var receiveTimeout = 1;
            var sendTimeout = 1;

            var remoteAddress = new EndpointAddress(new Uri(address), EndpointIdentity.CreateDnsIdentity("*"));

            if (isHttp)
            {
                var binding = new BasicHttpBinding
                {
                    MaxReceivedMessageSize = 2147483647,
                    MaxBufferSize = 2147483647,
                    MaxBufferPoolSize = 2147483647,
                    OpenTimeout = TimeSpan.FromMinutes(openTimeout),
                    CloseTimeout = TimeSpan.FromMinutes(closeTimeout),
                    ReceiveTimeout = TimeSpan.FromMinutes(receiveTimeout),
                    SendTimeout = TimeSpan.FromMinutes(sendTimeout),
                };

                if (!string.IsNullOrEmpty(sLog))
                {
                    binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                }
                
                var ws = new TemplateLibInternalClient(binding, remoteAddress);

                if (!string.IsNullOrEmpty(sLog) && ws.ClientCredentials != null)
                {
                    ws.ClientCredentials.UserName.UserName = sLog;
                    ws.ClientCredentials.UserName.Password = sPsw;
                }
                var customBinding = new CustomBinding(ws.Endpoint.Binding);
                var transportElement = customBinding.Elements.Find<HttpTransportBindingElement>();
                transportElement.KeepAliveEnabled = false;
                ws.Endpoint.Binding = customBinding;

                return ws;
            }

            if (isTcp)
            {
                var binding = new NetTcpBinding(SecurityMode.Message)
                {
                    MaxReceivedMessageSize = 2147483647,
                    MaxBufferSize = 2147483647,
                    MaxBufferPoolSize = 2147483647,
                    OpenTimeout = TimeSpan.FromMinutes(openTimeout),
                    CloseTimeout = TimeSpan.FromMinutes(closeTimeout),
                    ReceiveTimeout = TimeSpan.FromMinutes(receiveTimeout),
                    SendTimeout = TimeSpan.FromMinutes(sendTimeout),
                    ReaderQuotas =
                    {
                        MaxStringContentLength = 2147483647,
                        MaxArrayLength = 2147483647,
                        MaxBytesPerRead = 2147483647,
                        MaxNameTableCharCount = 2147483647
                    },
                };

                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
                binding.Security.Message.ClientCredentialType = MessageCredentialType.UserName;

                if (!string.IsNullOrEmpty(sLog))
                {
                    binding.Security.Mode = SecurityMode.Transport;
                    binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
                }

                var ws = new TemplateLibInternalClient(binding, remoteAddress);

                if (!string.IsNullOrEmpty(sLog) && ws.ClientCredentials != null)
                {
                    ws.ClientCredentials.UserName.UserName = sLog;
                    ws.ClientCredentials.UserName.Password = sPsw;
                }

                return ws;
            }

            throw new Exception($"not valid URL: '{address}'");
        }
    }
}
