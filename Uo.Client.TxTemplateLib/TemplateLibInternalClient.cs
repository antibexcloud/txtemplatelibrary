﻿using System;
using System.ServiceModel;
using Uo.Client.TxTemplateLib.TxTemplateLib.ServiceReference;

namespace Uo.Client.TxTemplateLib
{
    public class TemplateLibInternalClient : TxTemplateClient, IDisposable
    {
        public TemplateLibInternalClient(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public void Dispose()
        {
            try
            {
                if (State == CommunicationState.Opened)
                    Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Abort();
            }
        }
    }
}
