﻿using System.Runtime.Serialization;

namespace Uo.WcfService.TxTemplateLib.Logic
{
    [DataContract]
    public class LabelCountModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Count { get; set; }
    }
}
