﻿using System;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace Uo.WcfService.TxTemplateLib
{
    public class MyCustomUserNameValidator : UserNamePasswordValidator
    {
        private readonly string _log = "uouser";
        private readonly string _psw = "passc0de";

        public override void Validate(string userName, string password)
        {
            if (null == userName || null == password)
                throw new ArgumentNullException();

            if (userName.ToLower() != _log.ToLower() || password != _psw)
                throw new SecurityTokenException("Unknown Username or Password");
        }
    }
}