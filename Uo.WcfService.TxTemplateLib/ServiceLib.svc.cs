﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Uo.WcfService.TxTemplateLib.DbModel;
using Uo.WcfService.TxTemplateLib.Logic;

namespace Uo.WcfService.TxTemplateLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceLib" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceLib.svc or ServiceLib.svc.cs at the Solution Explorer and start debugging.
    public class ServiceLib : ITxTemplate
    {
        #region service

        private void AddAuditLog(TxTemplatesEntities dbConnect, string ip, string user,
            string table, string operation, string description)
        {
            dbConnect.AuditLogs.Add(new AuditLog
            {
                Date = DateTime.Now,
                Host = ip,
                User = user,
                Table = table,
                Operation = operation,
                Description = description,
            });
        }

        #endregion

        #region Labels

        public LabelModel[] GetLabels()
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    return dbConnect.Labels
                        .OrderBy(l => l.Name)
                        .Select(l => new LabelModel
                        {
                            Id = l.Id,
                            Name = l.Name,
                            Modified = l.Modified,
                        })
                        .ToArray();
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"GetLabels: {ex.Message}");
                    throw;
                }
        }

        public LabelCountModel[] GetLabelCounts()
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var res = dbConnect.Labels
                        .OrderBy(l => l.Name)
                        .Select(l => new LabelCountModel
                        {
                            Id = l.Id,
                            Name = l.Name,
                            Count = l.TextTemplateLabels.Count,
                        })
                        .ToArray();

                    res.First(c => c.Name == "All").Count = dbConnect.TextTemplates.Count();

                    return res;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"GetLabels: {ex.Message}");
                    throw;
                }
        }

        public bool AddLabel(string machineName, string user, string label)
        {
            if (string.IsNullOrEmpty(label))
                throw new Exception("Label can`t be empty!");

            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    if (dbConnect.Labels.Any(l => l.Name.ToLower() == label.ToLower()))
                        throw new Exception($"Label with name '{label}' already exists!");

                    dbConnect.Labels.Add(new Label
                    {
                        Name = label,
                        Modified = DateTime.Now,
                    });

                    AddAuditLog(dbConnect, machineName, user, "Label", "create", $"label name: '{label}'");

                    dbConnect.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"AddLabel: {ex.Message}");
                    throw;
                }
        }

        public bool RenameLabel(string machineName, string user, int id, string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new Exception("Label name is empty!");

            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.Labels.FirstOrDefault(l => l.Id == id);
                    if (find == null) return false;
                    if (find.Name.ToLower() == "all")
                        throw new Exception("Can`t rename label 'All'!");
                    if (dbConnect.Labels.Any(l => l.Name.ToLower() == name.ToLower()))
                        throw new Exception($"Label with name '{name}' already exists!");

                    AddAuditLog(dbConnect, machineName, user, "Label", "rename", $"old name: '{find.Name}', new name: '{name}'");

                    find.Name = name;

                    dbConnect.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"RenameLabel: {ex.Message}");
                    throw;
                }
        }

        public bool RemoveLabel(string machineName, string user, int id)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.Labels.FirstOrDefault(l => l.Id == id);
                    if (find == null) return false;
                    if (find.Name.ToLower() == "all")
                        throw new Exception("Can`t remove label 'All'!");

                    // remove
                    foreach (var textTemplateLabel in find.TextTemplateLabels.ToArray())
                        dbConnect.TextTemplateLabels.Remove(textTemplateLabel);
                    dbConnect.Labels.Remove(find);

                    AddAuditLog(dbConnect, machineName, user, "Label", "remove", $"label name: '{find.Name}'");
                    dbConnect.SaveChanges();

                    // check => all
                    var allLabel = dbConnect.Labels.FirstOrDefault(l => l.Name == "All");
                    if (allLabel == null)
                        throw new Exception("No label 'All'!");
                    foreach (var textTemplate in dbConnect.TextTemplates.Where(t => !t.TextTemplateLabels.Any()).ToArray())
                    {
                        dbConnect.TextTemplateLabels.Add(new TextTemplateLabel
                        {
                            Modified = DateTime.Now,
                            IdLabel = allLabel.Id,
                            IdTemplate = textTemplate.Id,
                            Label = allLabel,
                            TextTemplate = textTemplate,
                        });
                    }
                    dbConnect.SaveChanges();

                    // exit
                    return true;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"RemoveLabel: {ex.Message}");
                    throw;
                }
        }

        #endregion

        #region Templates

        public TxTemplateModel[] GetTextTemplates(int? idLabel)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    return dbConnect.TextTemplates
                        .Where(t => idLabel == null || t.TextTemplateLabels.Any(tl => tl.IdLabel == idLabel.Value))
                        .OrderBy(t => t.Name)
                        .Select(t => new TxTemplateModel
                        {
                            Id = t.Id,
                            Name = t.Name,
                            Modified = t.Modified,
                            Description = t.Description,
                            Labels = t.TextTemplateLabels
                                .Select(tl => new LabelModel
                                {
                                    Id = tl.Label.Id,
                                    Name = tl.Label.Name,
                                    Modified = tl.Label.Modified,
                                }),
                        })
                        .ToArray();
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"GetTextTemplates: {ex.Message}");
                    throw;
                }
        }

        public int AddTextTemplate(string machineName, string user, string name, byte[] fileBytes)
        {
            if (string.IsNullOrEmpty(name))
                throw new Exception("Name can`t be empty!");

            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var allLabel = dbConnect.Labels.FirstOrDefault(l => l.Name == "All");
                    if (allLabel == null)
                        throw new Exception("No label 'All'!");
                    //if (dbConnect.TextTemplates.Any(l => l.Name.ToLower() == name.ToLower()))
                    //    throw new Exception($"Template with name '{name}' already exists!");

                    var templateName = name;
                    //var idx = 1;
                    //while (dbConnect.TextTemplates.Any(t => t.Name.ToLower() == templateName.ToLower()))
                    //    templateName = $"{name}_{idx++}";

                    var template = new TextTemplate
                    {
                        Name = templateName,
                        Description = string.Empty,
                        TemplateArchive = fileBytes,
                        Modified = DateTime.Now,
                    };
                    dbConnect.TextTemplates.Add(template);

                    //
                    dbConnect.SaveChanges();
                    var findRes = dbConnect.TextTemplates.OrderByDescending(t => t.Id).FirstOrDefault(t => t.Name == templateName);

                    // label
                    findRes.TextTemplateLabels.Add(new TextTemplateLabel
                    {
                        Modified = DateTime.Now,
                        IdLabel = allLabel.Id,
                        IdTemplate = findRes.Id,
                        Label = allLabel,
                        TextTemplate = findRes,
                    });

                    // exit
                    AddAuditLog(dbConnect, machineName, user, "TextTemplate", "create", $"TextTemplate name: '{templateName}'");
                    dbConnect.SaveChanges();
                    return findRes.Id;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"AddTextTemplate: {ex.Message}");
                    throw;
                }
        }

        public bool RemoveTextTemplate(string machineName, string user, int id)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.TextTemplates.FirstOrDefault(t => t.Id == id);
                    if (find == null) return false;

                    foreach (var textTemplateLabel in find.TextTemplateLabels.ToArray())
                        dbConnect.TextTemplateLabels.Remove(textTemplateLabel);

                    dbConnect.TextTemplates.Remove(find);
                    AddAuditLog(dbConnect, machineName, user, "TextTemplate", "remove", $"TextTemplate name: '{find.Name}'");
                    dbConnect.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"RemoveTextTemplate: {ex.Message}");
                    throw;
                }
        }

        public bool UpdateTextTemplate(string machineName, string user, int id, string name, string description)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.TextTemplates.FirstOrDefault(t => t.Id == id);
                    if (find == null) return false;
                    if (string.IsNullOrEmpty(name))
                        throw new Exception("Name can`t be empty!");

                    AddAuditLog(dbConnect, machineName, user, "TextTemplate", "update",
                        //$"old name: '{find.Name}', new name = '{name}', " +
                        $"old Description: '{find.Description}', new Description: '{description}'");

                    //find.Name = name;
                    find.Description = description;
                    //find.Modified = DateTime.Now;

                    dbConnect.SaveChanges();
                    return true;

                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"UpdateTextTemplate: {ex.Message}");
                    throw;
                }
        }

        public bool SetTextTemplateLabels(string machineName, string user, int id, int[] labelIds)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.TextTemplates.FirstOrDefault(t => t.Id == id);
                    if (find == null) return false;

                    // all ids
                    var allIds = find.TextTemplateLabels.Select(tl => tl.IdLabel).ToList();

                    // remove
                    var forRemove = find.TextTemplateLabels
                        .Where(tl => labelIds.All(d => d != tl.IdLabel))
                        .ToArray();
                    if (forRemove.Any())
                    {
                        AddAuditLog(dbConnect, machineName, user, "TextTemplate", "remove labels",
                            $"TextTemplate name: '{find.Name}', " +
                            $"removed labels: [{string.Join(";", forRemove.Select(r => r.Label.Name))}]");

                        foreach (var label in forRemove)
                        {
                            dbConnect.TextTemplateLabels.Remove(label);
                            allIds.Remove(label.IdLabel);
                        }
                    }

                    // add
                    var forAdd = labelIds
                        .Where(tl => allIds.All(d => d != tl))
                        .ToArray();
                    if (forAdd.Any())
                    {
                        var labels = dbConnect.Labels.Where(l => forAdd.Any(d => d == l.Id)).ToArray();
                        foreach (var label in labels)
                        {
                            dbConnect.TextTemplateLabels.Add(new TextTemplateLabel
                            {
                                IdLabel = label.Id,
                                IdTemplate = find.Id,
                                Modified = DateTime.Now,
                                TextTemplate = find,
                                Label = label,
                            });
                        }
                        //
                        AddAuditLog(dbConnect, machineName, user, "TextTemplate", "add labels",
                            $"TextTemplate name: '{find.Name}', " +
                            $"added labels: [{string.Join(";", labels.Select(r => r.Name))}]");
                    }
                    // save
                    dbConnect.SaveChanges();

                    // check => all
                    var allLabel = dbConnect.Labels.FirstOrDefault(l => l.Name == "All");
                    if (allLabel == null)
                        throw new Exception("No label 'All'!");
                    foreach (var textTemplate in dbConnect.TextTemplates.Where(t => !t.TextTemplateLabels.Any()).ToArray())
                    {
                        dbConnect.TextTemplateLabels.Add(new TextTemplateLabel
                        {
                            Modified = DateTime.Now,
                            IdLabel = allLabel.Id,
                            IdTemplate = textTemplate.Id,
                            Label = allLabel,
                            TextTemplate = textTemplate,
                        });
                    }
                    dbConnect.SaveChanges();

                    // check => not all
                    foreach (var textTemplate in dbConnect.TextTemplates
                        .Where(t => t.TextTemplateLabels.Any(tl => tl.Label.Name == "All") && t.TextTemplateLabels.Count > 1)
                        .ToArray())
                    {
                        var findAll = textTemplate.TextTemplateLabels.FirstOrDefault(tl => tl.Label.Name == "All");
                        if (findAll != null)
                            dbConnect.TextTemplateLabels.Remove(findAll);
                    }
                    dbConnect.SaveChanges();

                    // exit
                    return true;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"SetTextTemplateLabels: {ex.Message}");
                    throw;
                }
        }

        public byte[] DownloadTextTemplate(string machineName, string user, int id)
        {
            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    var find = dbConnect.TextTemplates.FirstOrDefault(t => t.Id == id);
                    if (find != null)
                        AddAuditLog(dbConnect, machineName, user, "TextTemplate", "download", $"TextTemplate name: '{find.Name}'");
                    return find?.TemplateArchive;
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"DownloadTextTemplate: {ex.Message}");
                    throw;
                }
        }

        #endregion

        #region Audit

        public AuditModel[] GetAuditLog(int from, int count, string searchText)
        {
            var search = (searchText ?? string.Empty).ToLower();

            using (var dbConnect = TxTemplatesEntities.Open())
                try
                {
                    return dbConnect.AuditLogs
                        .Where(a => string.IsNullOrEmpty(search) ||
                                    a.Table.ToLower().Contains(search) ||
                                    a.Operation.ToLower().Contains(search) ||
                                    a.Host.ToLower().Contains(search) ||
                                    a.User.ToLower().Contains(search) ||
                                    a.Description.ToLower().Contains(search))
                        .OrderByDescending(a => a.Date)
                        .Skip(from).Take(count)
                        .Select(a => new AuditModel
                        {
                            Id = a.Id,
                            Date = a.Date,
                            Host = a.Host,
                            User = a.User,
                            Table = a.Table,
                            Operation = a.Operation,
                            Description = a.Description,
                        })
                        .ToArray();
                }
                catch (Exception ex)
                {
                    //TxTemplateLibService.Logger.Error($"GetAuditLog: {ex.Message}");
                    throw;
                }
        }

        #endregion
    }
}
