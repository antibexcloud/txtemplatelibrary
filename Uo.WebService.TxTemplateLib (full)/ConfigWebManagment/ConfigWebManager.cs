﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace Uo.WebService.TxTemplateLib.ConfigWebManagment
{
    public class ConfigWebManager
    {
        static ConfigWebManager()
        {
            Key = new Guid("57706676-F830-4B05-88A8-B03EE266B64A").ToByteArray();
            Vector = new Guid("32A08CC8-4A19-4CD1-B48C-1E0E61280F92").ToByteArray();
        }

        private static readonly byte[] Key;

        private static readonly byte[] Vector;

        public ConfigWebManager(string path)
        {
            _configLocation = path;
        }

        private readonly string _configLocation;
        public static Configuration _configuration = null;

        public void SetEncryptionFlag(string key, bool isEncrypted)
        {
            if (isEncrypted)
            {
                if (_encryptedKeysList.Contains(key))
                    return;
                _encryptedKeysList.Add(key);
            }
            else
                _encryptedKeysList.Remove(key);
        }

        private readonly IList<string> _encryptedKeysList = new List<string>();

        private static Configuration OpenConfigFile(string configPath)
        {
            if (_configuration != null) return _configuration;

            //web self open for read
            if (string.IsNullOrEmpty(configPath))
                return WebConfigurationManager.OpenWebConfiguration("~/");

            //editing config
            var vdm = new VirtualDirectoryMapping(configPath, true, "Web.config");
            var wcfm = new WebConfigurationFileMap();
            wcfm.VirtualDirectories.Add("/", vdm);
            return WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
        }

        public string this[string key]
        {
            get
            {
                var configuration = OpenConfigFile(_configLocation);
                var settingsPair = configuration.AppSettings.Settings[key];
                if (settingsPair == null)
                    throw new KeyNotFoundException(key);
                var rawData = settingsPair.Value;
                if (!_encryptedKeysList.Contains(key))
                    return rawData;
                if (!StringEncrypted(rawData))
                {
                    configuration.AppSettings.Settings[key].Value = Encrypt(rawData);
                    configuration.Save();
                    return rawData;
                }
                return Decrypt(rawData);
            }

            set
            {
                var configuration = OpenConfigFile(_configLocation);
                var settingsPair = configuration.AppSettings.Settings[key];
                if (settingsPair == null)
                    configuration.AppSettings.Settings.Add(key, "");
                configuration.AppSettings.Settings[key].Value = (!_encryptedKeysList.Contains(key)) ? value : Encrypt(value);
                configuration.Save();
            }
        }

        private static string Encrypt(string value)
        {
            byte[] data;
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write(value);
                data = stream.ToArray();
            }
            int rem;
            Math.DivRem(data.Length, 16, out rem);
            if (rem != 0)
            {
                var buf = new byte[data.Length + 16 - rem];
                for (var i = 0; i < data.Length; i++)
                    buf[i] = data[i];
                data = buf;
            }
            using (var transformer = new AesCryptoServiceProvider { Padding = PaddingMode.None }.CreateEncryptor(Key, Vector))
            using (var stream = new MemoryStream())
            using (var cryptoStream =
                new CryptoStream(stream, transformer, CryptoStreamMode.Write))
            {
                cryptoStream.Write(data, 0, data.Length);
                data = stream.ToArray();
            }
            return BitConverter.ToString(data)
                .Split('-')
                .Aggregate("", (str1, str2) => string.Format("{0} 0x{1}", str1, str2)).Trim();
        }

        private static bool StringEncrypted(string data)
        {
            return !string.IsNullOrEmpty(data) &&
                   new Regex(@"^((0x){1}[0-9A-Fa-f]{2} )*((0x){1}[0-9A-Fa-f]{2}){1}$").IsMatch(data);
        }

        private static string Decrypt(string value)
        {
            var data =
                value.Replace("0x", "")
                    .Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(e => Convert.ToByte(e, 16))
                    .ToArray();
            using (var transformer = new AesCryptoServiceProvider { Padding = PaddingMode.None }.CreateDecryptor(Key, Vector))
            using (var memStream = new MemoryStream(data))
            using (var cryptoStream = new CryptoStream(memStream, transformer, CryptoStreamMode.Read))
            {
                var buf = new byte[data.Length];
                cryptoStream.Read(buf, 0, buf.Length);
                data = buf;
            }
            using (var reader = new BinaryReader(new MemoryStream(data)))
                return reader.ReadString();
        }
    }
}