﻿using System;

namespace Uo.WebService.TxTemplateLib.ConfigWebManagment
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConfigurationPropertyAttribute : Attribute
    {
        public ConfigurationPropertyAttribute()
        {
            Key = null;
            IsEncrypted = false;
        }

        public ConfigurationPropertyAttribute(bool isEncrypted)
        {
            Key = null;
            IsEncrypted = isEncrypted;
        }

        public ConfigurationPropertyAttribute(string key, bool isEncrypted)
        {
            Key = key;
            IsEncrypted = isEncrypted;
        }

        public string Key { get; private set; }

        public bool IsEncrypted { get; private set; }
    }
}