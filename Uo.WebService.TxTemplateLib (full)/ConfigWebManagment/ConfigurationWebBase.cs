﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace Uo.WebService.TxTemplateLib.ConfigWebManagment
{
    public class ConfigurationWebBase
    {
        protected ConfigurationWebBase(string configForPath)
        {
            _configWebManager = new ConfigWebManager(configForPath);
            Init();
        }

        private readonly ConfigWebManager _configWebManager;

        protected TProperty GetPropertyValue<TProperty>(Expression<Func<TProperty>> projection)
        {
            var mapping = PropertyMappings[((MemberExpression)projection.Body).Member.Name];
            try
            {
                return ConvertTo<TProperty>(_configWebManager[mapping.Name]);
            }
            catch (KeyNotFoundException)
            {
                if (mapping.DefaultValueOnException)
                    return (TProperty)mapping.DefaultValue;
                throw;
            }
        }

        protected void SetPropertyValue<TProperty>(Expression<Func<TProperty>> projection, object val)
        {
            var mapping = PropertyMappings[((MemberExpression)projection.Body).Member.Name];
            try
            {
                var newValue = val.ToString();
                var oldValue = ConvertTo<TProperty>(_configWebManager[mapping.Name]).ToString();
                if (!oldValue.Equals(newValue))
                    _configWebManager[mapping.Name] = newValue;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        private T ConvertTo<T>(string data)
        {
            if (typeof(string) == typeof(T))
                return (T)(object)data;
            if (typeof(byte) == typeof(T))
                return (T)(object)byte.Parse(data);
            if (typeof(short) == typeof(T))
                return (T)(object)short.Parse(data);
            if (typeof(ushort) == typeof(T))
                return (T)(object)ushort.Parse(data);
            if (typeof(int) == typeof(T))
                return (T)(object)(int.Parse(data));
            if (typeof(uint) == typeof(T))
                return (T)(object)uint.Parse(data);
            if (typeof(ulong) == typeof(T))
                return (T)(object)ulong.Parse(data);
            if (typeof(bool) == typeof(T))
                return (T)(object)bool.Parse(data);
            if (typeof(T).IsEnum)
                return (T)Enum.Parse(typeof(T), data);

            throw new NotSupportedException();
        }

        public readonly IDictionary<string, PropertyMapInfo> PropertyMappings = new Dictionary<string, PropertyMapInfo>();

        private void Init()
        {
            var props = GetType().GetProperties();
            foreach (var info in props)
            {
                var attributes = info.GetCustomAttributes(true);

                var configAttr = attributes.OfType<ConfigurationPropertyAttribute>().FirstOrDefault();
                if (configAttr == null) continue;

                if (configAttr.IsEncrypted)
                    _configWebManager.SetEncryptionFlag(configAttr.Key ?? info.Name, true);

                var mapping = new PropertyMapInfo
                {
                    Name = configAttr.Key ?? info.Name,
                    TypeValue = info.PropertyType,
                    IsEncrypt = configAttr.IsEncrypted
                };

                var defValue = attributes.OfType<DefaultValueAttribute>().FirstOrDefault();
                if (defValue != null)
                {
                    mapping.DefaultValue = defValue.Value;
                    mapping.DefaultValueOnException = true;
                }

                var descValue = attributes.OfType<DisplayNameAttribute>().FirstOrDefault();
                if (descValue != null)
                    mapping.Description = descValue.DisplayName;

                PropertyMappings.Add(info.Name, mapping);
            }
        }
    }
}