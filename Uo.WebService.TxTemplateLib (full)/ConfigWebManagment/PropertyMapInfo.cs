﻿using System;

namespace Uo.WebService.TxTemplateLib.ConfigWebManagment
{
    public class PropertyMapInfo
    {
        private object _oldValue;

        public string Name { get; set; }

        public string Description { get; set; }

        public Type TypeValue { get; set; }

        public object DefaultValue { get; set; }

        public bool DefaultValueOnException { get; set; }

        public bool IsEncrypt { get; set; }

        public object OldValue
        {
            get { return _oldValue; }
            set
            {
                _oldValue = value;
                Value = value;
            }
        }

        public object Value { get; set; }

        public bool IsDirty
        {
            get { return OldValue != null && Value != null && !OldValue.ToString().Equals(Value.ToString()); }
        }
    }
}
