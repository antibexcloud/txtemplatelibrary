﻿namespace Uo.WebService.TxTemplateLib.Logic
{
    public interface ITxTemplate
    {
        #region Labels

        LabelModel[] GetLabels(string login, string password);

        LabelCountModel[] GetLabelCounts(string login, string password);

        int AddLabel(string login, string password, string machineName, string user, string label);

        bool RenameLabel(string login, string password, string machineName, string user, int id, string name);

        bool RemoveLabel(string login, string password, string machineName, string user, int id);

        #endregion

        #region Templates

        TxTemplateModel[] GetTextTemplates(string login, string password, int idLabel =0);

        int AddTextTemplate(string login, string password, string machineName, string user, string name, byte[] fileBytes);

        bool RemoveTextTemplate(string login, string password, string machineName, string user, int id);

        bool UpdateTextTemplate(string login, string password, string machineName, string user, int id, string description);

        bool SetTextTemplateLabels(string login, string password, string machineName, string user, int id, int[] labelIds);

        byte[] DownloadTextTemplate(string login, string password, string machineName, string user, int id);

        #endregion

        #region Audit

        AuditModel[] GetAuditLog(string login, string password);

        #endregion
    }
}
