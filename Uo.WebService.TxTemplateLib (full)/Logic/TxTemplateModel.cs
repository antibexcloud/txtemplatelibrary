﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Uo.WebService.TxTemplateLib.Logic
{
    [XmlInclude(typeof(LabelModel))]
    public class TxTemplateModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Modified { get; set; }

        public string Description { get; set; }

        public List<LabelModel> Labels { get; set; }
    }
}
