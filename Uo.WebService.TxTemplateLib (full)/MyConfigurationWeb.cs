﻿using System.ComponentModel;
using Uo.WebService.TxTemplateLib.ConfigWebManagment;

namespace Uo.WebService.TxTemplateLib
{
    public class MyConfigurationWeb : ConfigurationWebBase
    {
        public MyConfigurationWeb(string configForPath)
            : base(configForPath)
        {
        }

        [ConfigurationProperty(true)]
        public string DbConnectionString
        {
            get { return GetPropertyValue(() => DbConnectionString); }
            set { SetPropertyValue(() => DbConnectionString, value); }
        }

        [ConfigurationProperty("login", false)]
        [DefaultValue("uouser")]
        public string Login
        {
            get { return GetPropertyValue(() => Login); }
        }

        [ConfigurationProperty("password", false)]
        [DefaultValue("passc0de")]
        public string Password
        {
            get { return GetPropertyValue(() => Password); }
        }
    }
}