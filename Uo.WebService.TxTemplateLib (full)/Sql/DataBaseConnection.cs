﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Uo.WebService.TxTemplateLib.Logic;
using Uo.WebService.TxTemplateLib.Utils;

namespace Uo.WebService.TxTemplateLib.Sql
{
    public class DataBaseConnection : IDisposable
    {
        #region life circle

        private readonly SqlConnection _connection;
        private readonly string _connectString;

        private readonly SqlHelper _sqlHelperWork;

        public DataBaseConnection(string connectString)
        {
            _connectString = connectString;
            _connection = new SqlConnection(connectString);
            _connection.Open();

            _sqlHelperWork = new SqlHelper();
        }

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }

        #endregion

        public void AddAuditLog(string ip, string user,
            string table, string operation, string description)
        {
            using (var cmd = new SqlCommand(
                "insert AuditLog ([Date], [Host], [User], [Table], [Operation], Description)\r\n" +
                "  values (getdate(), @Host, @User, @Table, @Operation, @Description)", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Host", SqlDbType.NVarChar)).Value = ip.SafetySubstring(100);
                cmd.Parameters.Add(new SqlParameter("@User", SqlDbType.NVarChar)).Value = user.SafetySubstring(100);
                cmd.Parameters.Add(new SqlParameter("@Table", SqlDbType.NVarChar)).Value = table.SafetySubstring(32);
                cmd.Parameters.Add(new SqlParameter("@Operation", SqlDbType.NVarChar)).Value = operation.SafetySubstring(32);
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar)).Value = description;

                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        #region Labels

        public LabelModel[] GetLabels()
        {
            using (var cmd = new SqlCommand("select * from Label order by Name", _connection))
            {
                return _sqlHelperWork.TryFillDataTable(cmd).Rows.OfType<DataRow>()
                    .Select(row => new LabelModel
                    {
                        Id = ServiceConvert.GetInteger(row["Id"]),
                        Modified = ServiceConvert.GetDateTime(row["Modified"]),
                        Name = ServiceConvert.GetString(row["Name"]),
                    })
                    .ToArray();
            }
        }

        public LabelCountModel[] GetLabelCounts()
        {
            using (var cmd = new SqlCommand(
@"select T.Id, T.Name,
       count(Ttl.Id) as allCount
  from Label T left join
       TextTemplateLabel Ttl on Ttl.IdLabel = T.Id
  group by T.Id, T.Name
  order by T.Name", _connection))
            {
                return _sqlHelperWork.TryFillDataTable(cmd).Rows.OfType<DataRow>()
                    .Select(row => new LabelCountModel
                    {
                        Id = ServiceConvert.GetInteger(row["Id"]),
                        Name = ServiceConvert.GetString(row["Name"]),
                        Count = ServiceConvert.GetInteger(row["allCount"]),
                    })
                    .ToArray();
            }
        }

        public int GetTemplatesCounts()
        {
            using (var cmd = new SqlCommand(@"select count(Id) from TextTemplate", _connection))
            {
                return ServiceConvert.GetInteger(_sqlHelperWork.TryExecuteScalar(cmd));
            }
        }

        public int AddLabel(string label)
        {
            using (var cmd = new SqlCommand("insert [Label] (Name, Modified) values (@Name, getdate())\r\n" +
                                            "select SCOPE_IDENTITY() as Idnew", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = label.SafetySubstring(32);
                return ServiceConvert.GetInteger(_sqlHelperWork.TryExecuteScalar(cmd));
            }
        }

        public void RenameLabel(int id, string name)
        {
            using (var cmd = new SqlCommand("update [Label] set Name = @Name, Modified = getdate() where [Id] = @Id", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id;
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name.SafetySubstring(32);
                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        public void RemoveLabel(int id)
        {
            using (var cmd = new SqlCommand(
                "delete TextTemplateLabel where IdLabel = @Id\r\n" +
                "delete [Label] where [Id] = @Id", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id;
                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        #endregion

        #region Templates

        private List<LabelModel> GetTemplateLabels(int idTemplate)
        {
            using (var cmd = new SqlCommand(@"select Tl.*
  from TextTemplateLabel Ttl join
       [Label] Tl on Tl.Id = Ttl.IdLabel
  where Ttl.IdTemplate = @IdTemplate", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@IdTemplate", SqlDbType.Int)).Value = idTemplate;

                return _sqlHelperWork.TryFillDataTable(cmd).Rows.OfType<DataRow>()
                    .Select(row => new LabelModel
                    {
                        Id = ServiceConvert.GetInteger(row["Id"]),
                        Modified = ServiceConvert.GetDateTime(row["Modified"]),
                        Name = ServiceConvert.GetString(row["Name"]),
                    })
                    .ToList();
            }
        }

        public TxTemplateModel[] GetTextTemplates(int idLabel = 0)
        {
            using (var cmd = new SqlCommand(@"select distinct Tt.*
  from TextTemplate Tt left join
       TextTemplateLabel Ttl on Tt.Id = Ttl.IdTemplate
  where @idLabel=0 or Ttl.IdLabel = @idLabel", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@idLabel", SqlDbType.Int)).Value = idLabel;

                return _sqlHelperWork.TryFillDataTable(cmd).Rows.OfType<DataRow>()
                    .Select(row => new TxTemplateModel
                    {
                        Id = ServiceConvert.GetInteger(row["Id"]),
                        Modified = ServiceConvert.GetDateTime(row["Modified"]),
                        Name = ServiceConvert.GetString(row["Name"]),
                        Description = ServiceConvert.GetString(row["Description"]),
                        Labels = GetTemplateLabels(ServiceConvert.GetInteger(row["Id"])),
                    })
                    .ToArray();
            }
        }

        public int AddTextTemplate(string name, byte[] fileBytes)
        {
            using (var cmd = new SqlCommand("insert TextTemplate ([Name], Description, [Modified], TemplateArchive)" +
                                            "  values (@Name, '', getdate(), @TemplateArchive)\r\n" +
                                            "select SCOPE_IDENTITY() as Idnew", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar)).Value = name.SafetySubstring(150);
                cmd.Parameters.Add(new SqlParameter("@TemplateArchive", SqlDbType.VarBinary)).Value = fileBytes;

                return ServiceConvert.GetInteger(_sqlHelperWork.TryExecuteScalar(cmd));
            }
        }

        public void RemoveTextTemplate(int id)
        {
            using (var cmd = new SqlCommand(
                "delete TextTemplateLabel where IdTemplate = @Id\r\n" +
                "delete TextTemplate where [Id] = @Id", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id;

                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        public void UpdateTextTemplate(int id, string description)
        {
            using (var cmd = new SqlCommand("update TextTemplate set Description = @Description, Modified = getdate() where [Id] = @Id", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id;
                cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar)).Value = description;

                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        public void RemoveTextTemplateLabel(int id, int idLabel)
        {
            using (var cmd = new SqlCommand("delete TextTemplateLabel where IdTemplate = @IdTemplate and IdLabel = @idLabel", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@IdTemplate", SqlDbType.Int)).Value = id;
                cmd.Parameters.Add(new SqlParameter("@IdLabel", SqlDbType.Int)).Value = idLabel;

                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        public void SetTextTemplateLabel(int idTemplate, int idLabel)
        {
            using (var cmd = new SqlCommand("insert TextTemplateLabel (IdTemplate, IdLabel, Modified)" +
                                            "  values (@IdTemplate, @IdLabel, getdate())", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@IdTemplate", SqlDbType.Int)).Value = idTemplate;
                cmd.Parameters.Add(new SqlParameter("@IdLabel", SqlDbType.Int)).Value = idLabel;

                _sqlHelperWork.TryExecuteNonQuery(cmd);
            }
        }

        public byte[] DownloadTextTemplate(int id)
        {
            using (var cmd = new SqlCommand(@"select TemplateArchive from TextTemplate where [Id] = @id", _connection))
            {
                cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.Int)).Value = id;
                return ServiceConvert.GetBytes(_sqlHelperWork.TryExecuteScalar(cmd));
            }
        }

        #endregion

        #region Audit

        public AuditModel[] GetAuditLog()
        {
            using (var cmd = new SqlCommand("select * from AuditLog order by Date", _connection))
            {
                return _sqlHelperWork.TryFillDataTable(cmd).Rows.OfType<DataRow>()
                    .Select(row => new AuditModel
                    {
                        Id = ServiceConvert.GetInteger(row["Id"]),
                        Date = ServiceConvert.GetDateTime(row["Date"]),
                        Operation = ServiceConvert.GetString(row["Operation"]),
                        Table = ServiceConvert.GetString(row["Table"]),
                        Host = ServiceConvert.GetString(row["Host"]),
                        User = ServiceConvert.GetString(row["User"]),
                        Description = ServiceConvert.GetString(row["Description"]),
                    })
                    .ToArray();
            }
        }

        #endregion
    }
}
