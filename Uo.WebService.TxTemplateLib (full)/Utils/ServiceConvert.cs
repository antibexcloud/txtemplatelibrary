﻿using System;
using System.Data.SqlTypes;

namespace Uo.WebService.TxTemplateLib.Utils
{
    public static class ServiceConvert
    {
        private static readonly DateTime MinDateValue = (DateTime)SqlDateTime.MinValue;

        public static string ToLogDateTime(this DateTime source)
        {
            var kind = source.Kind;
            if (kind == DateTimeKind.Unspecified)
                source = DateTime.SpecifyKind(source, DateTimeKind.Local);
            if (kind == DateTimeKind.Local)
                return source.ToString("yyyy-MM-dd T HH:mm:ss");
            return source.ToString("yyyy-MM-ddTHH:mm:ss") + "Z";
        }

        private static object FixDbNull(object value)
        {
            return value is DBNull ? null : value;
        }

        public static string GetString(object aInput, string defaultValue="")
        {
            return (FixDbNull(aInput) != null) ? Convert.ToString(aInput) : defaultValue;
        }

        public static int GetInteger(object aInput, int defaultValue = 0)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToInt32(aInput) : defaultValue;
        }

        public static DateTime GetDateTime(object aInput)
        {
            var dt = (FixDbNull(aInput) != null) ? Convert.ToDateTime(aInput) : MinDateValue;
            if (dt.Kind == DateTimeKind.Unspecified)
                dt = DateTime.SpecifyKind(dt, DateTimeKind.Local);
            return dt;
        }

        public static byte[] GetBytes(object aInput)
        {
            try
            {
                if (FixDbNull(aInput) == null) return null;
                return aInput as byte[];
            }
            catch
            {
                return null;
            }
        }

        public static string SafetySubstring(this string str, int maxLengtth)
        {
            return str?.Substring(0, Math.Min(str.Length, maxLengtth)) ?? "";
        }
    }
}
