﻿using System;

namespace Uo.WebService.TxTemplateLib.Logic
{
    public class AuditModel
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Host { get; set; }

        public string User { get; set; }

        public string Table { get; set; }

        public string Operation { get; set; }

        public string Description { get; set; }
    }
}
