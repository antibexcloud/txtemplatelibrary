﻿namespace Uo.WebService.TxTemplateLib.Logic
{
    public class LabelCountModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }
    }
}
