﻿using System;

namespace Uo.WebService.TxTemplateLib.Logic
{
    public class LabelModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Modified { get; set; }
    }
}
