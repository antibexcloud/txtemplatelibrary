﻿using System.Data;
using System.Data.SqlClient;

namespace Uo.WebService.TxTemplateLib.Sql
{
    public class SqlHelper
    {
        public int TryExecuteNonQuery(SqlCommand cmd)
        {
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();
                var res = cmd.ExecuteNonQuery();
                return res;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public object TryExecuteScalar(SqlCommand cmd)
        {
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();
                var res = cmd.ExecuteScalar();
                return res;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataTable TryFillDataTable(SqlCommand cmd)
        {
            var table = new DataTable();
            using (var adapter = new SqlDataAdapter(cmd))
                adapter.Fill(table);
            return table;
        }
    }
}
