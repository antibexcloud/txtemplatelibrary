﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Services;
using System.Xml.Serialization;
using Uo.WebService.TxTemplateLib.Logic;
using Uo.WebService.TxTemplateLib.Sql;

namespace Uo.WebService.TxTemplateLib
{
    // development
    // http://localhost/Uo.WebService.TxTemplateLib/WebServiceTxTemplateLibrary.asmx
    // http://localhost/Uo.WebService.TxTemplateLib/WebServiceTxTemplateLibrary.asmx?WSDL

    // work
    // http://70.33.247.115/plesk-site-preview/txtemplates.antibexsoftware.ca/
    // http://70.33.247.115/plesk-site-preview/txtemplates.antibexsoftware.ca/WebServiceTxTemplateLibrary.asmx?WSDL

    // rezerv
    // http://70.33.247.115/plesk-site-preview/templates.antibexsoftware.ca/WebServiceTxTemplateLibrary.asmx
    // http://70.33.247.115/plesk-site-preview/templates.antibexsoftware.ca/WebServiceTxTemplateLibrary.asmx?WSDL

    [WebService(Namespace = "http://Abtibex.org/", Description = "TX Template Library", Name = "TxTemplateLib")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [XmlInclude(typeof(AuditModel))]
    [XmlInclude(typeof(LabelCountModel))]
    [XmlInclude(typeof(LabelModel))]
    [XmlInclude(typeof(TxTemplateModel))]
    [System.ComponentModel.ToolboxItem(false)]
    public class WebServiceTxTemplateLibrary : System.Web.Services.WebService, ITxTemplate
    {
        private static MyConfigurationWeb _configuration;
        private static MyConfigurationWeb Configuration => _configuration ?? (_configuration = new MyConfigurationWeb());

        #region service

        private void CheckLogin(string login, string password)
        {
            if (Configuration.Login.ToLower() != (login ?? string.Empty).ToLower())
                throw new Exception("Bag user login");
            if (Configuration.Password.ToLower() != (password ?? string.Empty).ToLower())
                throw new Exception("Bag user password");
        }

        #endregion

        #region Labels

        [WebMethod]
        public LabelModel[] GetLabels(string login, string password)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                return dbConnect.GetLabels();
            }
        }

        [WebMethod]
        public LabelCountModel[] GetLabelCounts(string login, string password)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var res = dbConnect.GetLabelCounts();
                res.Add(new LabelCountModel
                {
                    Id = 99999,
                    Name = "All",
                    Count = dbConnect.GetTemplatesCounts(),
                });
                return res.ToArray();
            }
        }

        [WebMethod]
        public int AddLabel(string login, string password, string machineName, string user, string label)
        {
            CheckLogin(login, password);

            if (string.IsNullOrEmpty(label))
                throw new Exception("Label can`t be empty!");

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                if (dbConnect.GetLabels().Any(l => l.Name.ToLower() == label.ToLower()))
                    throw new Exception($"Label with name '{label}' already exists!");

                var idResult = dbConnect.AddLabel(label);
                dbConnect.AddAuditLog(machineName, user, "Label", "create", $"\"{label}\" has been created");
                return idResult;
            }
        }

        [WebMethod]
        public bool RenameLabel(string login, string password, string machineName, string user, int id, string name)
        {
            CheckLogin(login, password);

            if (string.IsNullOrEmpty(name))
                throw new Exception("Label name is empty!");

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var allLabels = dbConnect.GetLabels();

                var find = allLabels.FirstOrDefault(l => l.Id == id);
                if (find == null) return false;

                if (allLabels.Any(l => l.Name.ToLower() == name.ToLower()))
                    throw new Exception($"Label with name '{name}' already exists!");

                dbConnect.RenameLabel(id, name);
                dbConnect.AddAuditLog(machineName, user, "Label", "modify",
                    $"\"{find.Name}\" has been modified from \"{find.Name}\" to \"{name}\"");
                return true;
            }
        }

        [WebMethod]
        public bool RemoveLabel(string login, string password, string machineName, string user, int id)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var allLabels = dbConnect.GetLabels();

                var find = allLabels.FirstOrDefault(l => l.Id == id);
                if (find == null) return false;

                // remove
                dbConnect.RemoveLabel(find.Id);
                dbConnect.AddAuditLog(machineName, user, "Label", "delete", $"'{find.Name}' has been deleted");

                // exit
                return true;
            }
        }

        #endregion

        #region Templates

        [WebMethod]
        public TxTemplateModel[] GetTextTemplates(string login, string password, int idLabel=0)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                return dbConnect.GetTextTemplates(idLabel);
            }
        }

        [WebMethod]
        public int AddTextTemplate(string login, string password, string machineName, string user,
            string name, byte[] fileBytes)
        {
            CheckLogin(login, password);

            if (string.IsNullOrEmpty(name))
                throw new Exception("Name can`t be empty!");

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var templateName = name;
                var idResult = dbConnect.AddTextTemplate(name, fileBytes);
                if (idResult <= 0)
                    return 0;

                // exit
                dbConnect.AddAuditLog(machineName, user, "TextTemplate", "upload", $"\"{templateName}\" has been uploaded");
                return idResult;
            }
        }

        [WebMethod]
        public bool RemoveTextTemplate(string login, string password, string machineName, string user, int id)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var find = dbConnect.GetTextTemplates().FirstOrDefault(t => t.Id == id);
                if (find == null) return false;

                dbConnect.RemoveTextTemplate(id);
                dbConnect.AddAuditLog(machineName, user, "TextTemplate", "delete", $"\"{find.Name}\" has been deleted");
                return true;
            }
        }

        [WebMethod]
        public bool UpdateTextTemplate(string login, string password, string machineName, string user,
            int id, string description)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var find = dbConnect.GetTextTemplates().FirstOrDefault(t => t.Id == id);
                if (find == null) return false;

                dbConnect.UpdateTextTemplate(id, description);
                dbConnect.AddAuditLog(machineName, user, "TextTemplate", "modify",
                    $"\"{find.Name}\" description has been modified from \"{find.Description}\" to \"{description}\"");
                return true;
            }
        }

        [WebMethod]
        public bool RenameTextTemplate(string login, string password, string machineName, string user,
            int id, string name, string description, byte[] fileBytes)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var find = dbConnect.GetTextTemplates().FirstOrDefault(t => t.Id == id);
                if (find == null) return false;

                dbConnect.RenameTextTemplate(id, name, description, fileBytes);
                dbConnect.AddAuditLog(machineName, user, "TextTemplate", "modify",
                    $"\"{find.Name}\" renamed to \"{name}\", description has been modified from \"{find.Description}\" to \"{description}\"");
                return true;
            }
        }

        [WebMethod]
        public bool SetTextTemplateLabels(string login, string password, string machineName, string user,
            int id, int[] labelIds)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var find = dbConnect.GetTextTemplates().FirstOrDefault(t => t.Id == id);
                if (find == null) return false;

                // all ids
                var allIds = find.Labels.Select(tl => tl.Id).ToList();
                var allLabels = dbConnect.GetLabels();

                // remove
                var forRemove = find.Labels
                    .Where(tl => labelIds.All(d => d != tl.Id))
                    .ToArray();
                if (forRemove.Any())
                {
                    if (forRemove.Length == 1)
                    {
                        var label = forRemove.First();
                        dbConnect.AddAuditLog(machineName, user, "TextTemplate", "label",
                            $"Label \"{label.Name}\" has been removed from \"{find.Name}\"");
                    }
                    else
                    {
                        dbConnect.AddAuditLog(machineName, user, "TextTemplate", "label",
                            $"Labels \"{string.Join(";", forRemove.Select(r => r.Name).ToArray())}\" have been removed from \"{find.Name}\"");
                    }

                    foreach (var label in forRemove)
                    {
                        dbConnect.RemoveTextTemplateLabel(id, label.Id);
                        allIds.Remove(label.Id);
                    }
                }

                // add
                var forAdd = labelIds
                    .Where(tl => allIds.All(d => d != tl))
                    .ToArray();
                if (forAdd.Any())
                {
                    var labels = allLabels.Where(l => forAdd.Any(d => d == l.Id)).ToArray();
                    foreach (var label in labels)
                        dbConnect.SetTextTemplateLabel(id, label.Id);
                    //
                    if (labels.Length == 1)
                    {
                        var label = labels.First();
                        dbConnect.AddAuditLog(machineName, user, "TextTemplate", "label",
                            $"Label \"{label.Name}\" has been added to \"{find.Name}\"");
                    }
                    else
                    {
                        dbConnect.AddAuditLog(machineName, user, "TextTemplate", "label",
                            $"Labels \"{string.Join(";", labels.Select(r => r.Name).ToArray())}\" have been added to \"{find.Name}\"");
                    }
                }

                // exit
                return true;
            }
        }

        [WebMethod]
        public byte[] DownloadTextTemplate(string login, string password, string machineName, string user, int id)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                var find = dbConnect.GetTextTemplates().FirstOrDefault(t => t.Id == id);
                if (find != null)
                    dbConnect.AddAuditLog(machineName, user, "TextTemplate", "download", $"\"{find.Name}\" has been downloaded");
                return find != null
                    ? dbConnect.DownloadTextTemplate(id)
                    : null;
            }
        }

        #endregion

        #region Audit

        [WebMethod]
        public AuditModel[] GetAuditLog(string login, string password)
        {
            CheckLogin(login, password);

            using (var dbConnect = new DataBaseConnection(Configuration.DbConnectionString))
            {
                return dbConnect.GetAuditLog();
            }
        }

        #endregion
    }
}
