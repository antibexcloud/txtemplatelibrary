﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Uo.WinService.TxTemplateLib.DbModel
{
    public partial class TxTemplatesEntities
    {
        private TxTemplatesEntities(string nameOrConnectionString) : base(nameOrConnectionString)
        {

        }

        private static TxTemplatesEntities Open(string serverName, string dbName, bool integratedSecurity, string login, string password)
        {
            var entityBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder
            {
                ProviderConnectionString = $"data source={serverName};initial catalog={dbName};MultipleActiveResultSets=True;App=EntityFramework;",
                Provider = "System.Data.SqlClient",
                Metadata = @"res://*/DbModel.LibModel.csdl|res://*/DbModel.LibModel.ssdl|res://*/DbModel.LibModel.msl"
            };

            entityBuilder.ProviderConnectionString +=
            (integratedSecurity
                ? "Integrated Security=True;Persist Security Info=False"
                : $"Persist security info=True;User id={login};Password={password};");

            var connectionString = entityBuilder.ConnectionString;

            return new TxTemplatesEntities(connectionString);
        }

        public static TxTemplatesEntities Open()
        {
            try
            {
                var connectString = ConfigurationManager.AppSettings["DbConnectionString"];

                // prepare data
                var buider = new SqlConnectionStringBuilder(connectString);

                var name = buider.InitialCatalog;
                var serverName = buider.DataSource;
                var login = buider.UserID;
                var password = buider.Password;
                var integratedSecurity = buider.IntegratedSecurity;

                // open
                return Open(serverName, name, integratedSecurity, login, password);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
