﻿using System;
using System.Runtime.Serialization;

namespace Uo.WinService.TxTemplateLib.Logic
{
    [DataContract]
    public class AuditModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Host { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Table { get; set; }

        [DataMember]
        public string Operation { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
