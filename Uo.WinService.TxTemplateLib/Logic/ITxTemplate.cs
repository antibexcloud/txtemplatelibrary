﻿using System.ServiceModel;

namespace Uo.WinService.TxTemplateLib.Logic
{
    [ServiceContract]
    public interface ITxTemplate
    {
        #region Labels

        [OperationContract]
        LabelModel[] GetLabels();

        [OperationContract]
        LabelCountModel[] GetLabelCounts();

        [OperationContract]
        bool AddLabel(string machineName, string user, string label);

        [OperationContract]
        bool RenameLabel(string machineName, string user, int id, string name);

        [OperationContract]
        bool RemoveLabel(string machineName, string user, int id);

        #endregion

        #region Templates

        [OperationContract]
        TxTemplateModel[] GetTextTemplates(int? idLabel);

        [OperationContract]
        int AddTextTemplate(string machineName, string user, string name, byte[] fileBytes);

        [OperationContract]
        bool RemoveTextTemplate(string machineName, string user, int id);

        [OperationContract]
        bool UpdateTextTemplate(string machineName, string user, int id, string name, string description);

        [OperationContract]
        bool SetTextTemplateLabels(string machineName, string user, int id, int[] labelIds);

        [OperationContract]
        byte[] DownloadTextTemplate(string machineName, string user, int id);

        #endregion

        #region Audit

        [OperationContract]
        AuditModel[] GetAuditLog(int from, int count, string searchText);

        #endregion
    }
}
