﻿using System;
using System.Runtime.Serialization;

namespace Uo.WinService.TxTemplateLib.Logic
{
    [DataContract]
    public class LabelModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }
    }
}
