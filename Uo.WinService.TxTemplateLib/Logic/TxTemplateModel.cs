﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Uo.WinService.TxTemplateLib.Logic
{
    [DataContract]
    public class TxTemplateModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public IEnumerable<LabelModel> Labels { get; set; }
    }
}
