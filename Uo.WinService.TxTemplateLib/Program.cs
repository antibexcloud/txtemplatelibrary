﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;

namespace Uo.WinService.TxTemplateLib
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            var servicesToRun = new ServiceBase[] { new TxTemplateLibService() };

            if (Environment.UserInteractive)
            { // для запуска в режиме консоли
                AllocConsole();

                var type = typeof(ServiceBase);
                const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
                var method = type.GetMethod("OnStart", flags);
                foreach (var service in servicesToRun)
                    method.Invoke(service, new object[] { args });

                Console.WriteLine(@"Press any key to exit");
                Console.ReadKey();

                var methodStop = type.GetMethod("OnStop", flags);
                foreach (var service in servicesToRun)
                    methodStop.Invoke(service, null);
            }
            else
            {
                var cmdLine = Environment.CommandLine.Remove(Environment.CommandLine.Length - 2, 2).Remove(0, 1);
                var workDir = Path.GetDirectoryName(cmdLine);
                Environment.CurrentDirectory = workDir;
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
