﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;

namespace Uo.WinService.TxTemplateLib.ServerHosting
{
    public static class NetService
    {
        #region Constans

        private const int ShortTimeOut1 = 5;
        private const int ShortTimeOut2 = 20;
        private const int LongTimeOut1 = 5;
        private const int LongTimeOut2 = 30;

        #endregion

        /// <summary>
        /// get IP-address by hostname
        /// </summary>
        /// <returns>IP-address</returns>
        public static string GetIp(string hostName)
        {
            var host = Dns.GetHostEntry(hostName);
            var localIp = host.AddressList.FirstOrDefault(ip => ip.AddressFamily.ToString().Equals("InterNetwork"));
            return localIp?.ToString() ?? "";
        }

        #region bindings

        /// <summary>
        /// create binding
        /// </summary>
        /// <returns>binding</returns>
        public static NetTcpBinding GetTcpBinding()
        {
            var binding = new NetTcpBinding(SecurityMode.Transport)
            {
                MaxBufferPoolSize = 2147483647,
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                MaxConnections = 1024,
                ReaderQuotas =
                {
                    MaxArrayLength = 2147483647,
                    MaxBytesPerRead = 2147483647,
                    MaxStringContentLength = 2147483647
                },
                TransactionFlow = false,
                //PortSharingEnabled = true,
                OpenTimeout = TimeSpan.FromSeconds(ShortTimeOut1),
                CloseTimeout = TimeSpan.FromSeconds(ShortTimeOut1),
                SendTimeout = TimeSpan.FromSeconds(ShortTimeOut2),
                ReceiveTimeout = TimeSpan.FromSeconds(ShortTimeOut2)
            };

            return binding;
        }

        /// <summary>
        /// create binding
        /// </summary>
        /// <returns>binding</returns>
        public static BasicHttpBinding GetBasicHttpBinding()
        {
            var binding = new BasicHttpBinding
            {
                MaxBufferPoolSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                ReaderQuotas =
                {
                    MaxArrayLength = 2147483647,
                    MaxBytesPerRead = 2147483647,
                    MaxStringContentLength = 2147483647
                },
                OpenTimeout = TimeSpan.FromSeconds(LongTimeOut1),
                CloseTimeout = TimeSpan.FromSeconds(LongTimeOut1),
                SendTimeout = TimeSpan.FromSeconds(LongTimeOut2),
                ReceiveTimeout = TimeSpan.FromSeconds(LongTimeOut2)
            };

            return binding;
        }

        #endregion
    }
}
