﻿using System;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;

namespace Uo.WinService.TxTemplateLib.ServerHosting
{
    public class MyCustomUserNameValidator : UserNamePasswordValidator
    {
        private readonly string _log;
        private readonly string _psw;

        public MyCustomUserNameValidator(string log, string psw)
        {
            _log = log; _psw = psw;
        }

        public override void Validate(string userName, string password)
        {
            if (null == userName || null == password)
                throw new ArgumentNullException();

            if (userName.ToLower() != _log.ToLower() || password != _psw)
                throw new SecurityTokenException("Unknown Username or Password");
        }
    }


    public sealed class WcfServiceHost : IDisposable
    {
        private readonly ServiceHost _host;

        
        public WcfServiceHost(System.Type interfaceType, object singleton, Uri[] hostingUri,
            string login, string password)
        {
            if (hostingUri == null) throw new ArgumentNullException(nameof(hostingUri));

            // create host
            _host = new ServiceHost(singleton, hostingUri);

            _host.Description.Behaviors.Find<ServiceDebugBehavior>().IncludeExceptionDetailInFaults = true;
            _host.Description.Behaviors.Add(new ServiceThrottlingBehavior
            {
                MaxConcurrentCalls = 128,
                MaxConcurrentInstances = 128,
                MaxConcurrentSessions = 128
            });

            // add endpoints
            foreach (var uri in hostingUri)
            {
                try
                {
                    Binding binding;
                    switch (uri.Scheme)
                    {
                        case "http":
                            binding = NetService.GetBasicHttpBinding();
                            var httpBinding = (binding as BasicHttpBinding);
                            if (httpBinding != null)
                            {
                                httpBinding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                                httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                            }
                            _host.Description.Behaviors.Add(new ServiceMetadataBehavior { HttpGetEnabled = true });
                            if (!string.IsNullOrEmpty(login))
                            {
                                // Use UserName Message Authentication
                                ((BasicHttpBinding)binding).Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;

                                // Use Custom UserName Authentication Mode
                                _host.Credentials.UserNameAuthentication.UserNamePasswordValidationMode = UserNamePasswordValidationMode.Custom;
                                _host.Credentials.UserNameAuthentication.CustomUserNamePasswordValidator = new MyCustomUserNameValidator(login, password);
                            }
                            break;

                        case "net.tcp":
                            binding = NetService.GetTcpBinding();
                            break;

                        default:
                            throw new NotSupportedException();
                    }
                    _host.AddServiceEndpoint(interfaceType, binding, uri);
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch { }
            }

            // open
            _host.Open();
        }

        public void Dispose()
        {
            try
            {
                _host.Close();
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception) { }
        }
    }
}
