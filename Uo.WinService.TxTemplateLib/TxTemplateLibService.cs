﻿using System;
using System.ServiceProcess;
using System.Threading;
using NLog;

namespace Uo.WinService.TxTemplateLib
{
    public partial class TxTemplateLibService : ServiceBase
    {
        public static readonly Logger Logger = LogManager.GetLogger("main");

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            var e = (Exception)args.ExceptionObject;
            Logger.Error("UnhandledExceptionEventHandler", e.InnerException ?? e);
            if (Environment.UserInteractive)
                Environment.Exit(0);
        }

        public TxTemplateLibService()
        {
            InitializeComponent();

            AppDomain.CurrentDomain.UnhandledException += MyHandler;
        }

        protected override void OnStart(string[] args)
        {
            Logger.Debug("Service started");

            if (Environment.UserInteractive)
                InitServer();
            else
                ThreadPool.QueueUserWorkItem(obj => InitServer());
        }

        protected override void OnStop()
        {
            if (_server == null) return;

            try
            {
                Logger.Debug("Service stoped");
                ((IDisposable)_server).Dispose();
            }
            catch (Exception ex)
            {
                Logger.Error("stop service exception", ex);
            }
            finally
            {
                _server = null;
            }
        }

        private void InitServer()
        {
            try
            {
                _server = new ServerWork();
                if (_server.Init()) return;

                Logger.Info("bad init!");
                OnStop();
            }
            catch (Exception ex)
            {
                _server = null;
                Logger.Error("start service exception", ex);
            }

            //exit
            if (Environment.UserInteractive)
                Environment.Exit(0);
            else
                Stop();
        }

        private ServerWork _server;
    }
}
