﻿using System;
using System.Windows;
using Uo.Client.TxTemplateLib;

namespace Wpf.Test.TxTemplateLib
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonTestOnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var client = TemplateLibClient.GetClient())
                {
                    var res = client.GetLabels();
                    var cnt = res?.Length ?? 0;
                    MessageBox.Show($"{cnt}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
